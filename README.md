# Purpose of This Repository
This repository works to motion correct contrast-enhanced ultrasound.

# How to Use This Repository
1. Install all necessary dependencies listed in **Necessary Dependencies to be Installed**
2. Create the .nii.gz data from xml2nii.py
3. Run MotionCorr.sh using the explanation for the inputs listed in **How to Run the Code**
4. The results of the code can be visualized with software such as ITK-Snap

# Necessary Dependencies to be Installed
## FSL
[https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSL]
## FreeSurfer
[https://surfer.nmr.mgh.harvard.edu/fswiki/DownloadAndInstall]
## NiftyReg
[http://cmictig.cs.ucl.ac.uk/wiki/index.php/NiftyReg_install]

# Necessary Data Needed to Run the Code
1. Xml and Raw data that will be converted to .nii.gz through xml2nii.py
2. Nifti data frames that must be named orgvol*.nii.gz

# How to Run the Code
In order for the code to run, there must be a directory containing the nifty data to be motion corrected and 
the masked nifty data created with xml2nii. The name of this directory must correspond to the *WORKDIR* input
when running the code. The nifty data must be named *NAME*.nii.gz, and the masked nifty data must be named *NAME*\_mask\_\*.nii.gz.
*NAME* must correspond to the *NAME* input when running the code. Inside the *WORKDIR* directory, there must also be
another directory called *NAME*. Inside this directory, there must be a directory named "split" which holds the frames
for the nifty data. 
## Syntax for Running the Code
./MotionCorr.sh *WORKDIR* *NAME* *WL*

1. *WORKDIR* refers to the name of the directory containing the nifty data and frames

2. *NAME* refers to the name of the nifty data

3. *WL* refers to the number of windows created for the window average (Recommended number is 5)